package CRM_Tab;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import Common_Methods.CommonMethods;

public class All_Subscriber_tab extends CommonMethods{
	
	@BeforeClass()
	public void CRM() throws InterruptedException
	{
		Login();
		SearchCustomerByAccountNumber(AccNo);
		
	}
	
	@AfterClass(alwaysRun=true)
	public void stop()
	{
	  driver.quit();
		
	}
	
	@Test
	public void a_View_AllSubscriber_tab() throws InterruptedException
	{
		 View_AllSubscriberTab();
	}

	@Test
	public void b_Access_AllSubscriber_tab() throws InterruptedException
	{
		Access_All_SubscriberDetailsTab();
	}
	
	@Test
	public void c_View_Subscriber_details() throws InterruptedException
	{
		view_SubscriberDetails();
	}

	
	
/////////////////////////////////////////////////////////////////////////////////////////////////////
//**************************** Method to view all subscriber details tab ******************************
 public void View_AllSubscriberTab()
 {
	WebElement allSub= driver.findElement(By.xpath("/html[1]/body[1]/form[1]/div[3]/div[1]/div[3]/div[2]/ul[1]/li[4]/a[1]"));
	
	Boolean Adetails= allSub.isDisplayed();
	
	Assert.assertTrue(Adetails);
 }
 
 //*************************** Method to access address details tab ****************************
 public void Access_All_SubscriberDetailsTab() throws InterruptedException
 {
	 Thread.sleep(1500);
	
	 WebElement address= driver.findElement(By.xpath("/html[1]/body[1]/form[1]/div[3]/div[1]/div[3]/div[2]/ul[1]/li[4]/a[1]"));
	 
	 Actions act= new Actions(driver);
	 
	 act.moveToElement(address).doubleClick().build().perform();
	 
	 act.moveToElement(address).doubleClick().build().perform();
 }
 
 //************************** Method to view address details of customer **************************
 public void view_SubscriberDetails() throws InterruptedException
 {
	 Thread.sleep(1500);
	 
	 WebElement Subscriber= driver.findElement(By.xpath("/html[1]/body[1]/form[1]/div[3]/div[1]/div[3]/div[2]/div[4]/div[1]/div[1]"));
		
	 Boolean Sdetails= Subscriber.isDisplayed();
		
	 Assert.assertTrue(Sdetails); 
 }

}
