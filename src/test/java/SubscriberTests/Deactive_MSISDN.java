package SubscriberTests;

import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import Common_Methods.CommonMethods;

public class Deactive_MSISDN extends CommonMethods {
	
	public static String msisdn= "27645774286";
	
	@BeforeClass
	public void start() throws InterruptedException
	{
		Login();
		Search_By_MSISDN(msisdn);
		Thread.sleep(500);
	       	
	}
	
	@AfterClass(alwaysRun=true)
	public void end()
	{
		driver.close();
	}
	
	@Test
	public void aClick_DeactiveTab() throws InterruptedException
	{
		Thread.sleep(1200);
		click_deactive();
		
	}
	
	@Test
	public void b_confirm_MSISDN() throws InterruptedException
	{
		Thread.sleep(1200);
		ReEnterMSISDN();
	}
	
	@Test
	public void c_Confirm() throws InterruptedException
	{
		Thread.sleep(1200);
		confirm();
	}
	
	@Test
	public void d_Submit_deletionRequest() throws InterruptedException
	{
		Thread.sleep(1200);
		SUBMIT();
	}
	
	@Test
	public void e_CheckSuccessMesssage() throws InterruptedException
	{
		Thread.sleep(1200);
		CheckSuccessMsg();
	}
	
	@Test
	public void f_View_SubscriberInfoAfterDeactivation()
	{
		Search_By_MSISDN(msisdn);
		View_subscriber_info();
	}
	
	
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//********************************************* Method to search subscriber by MSISDN ****************************************
public void Search_By_MSISDN(String msisdn)
{
 try {
	 Select dropdown = new Select(driver.findElement(By.id("P0_SEARCH_TYPE")));
	
	 dropdown.selectByVisibleText("MSISDN");
	
	 driver.findElement(By.id("P0_SEARCH_VALUE")).clear();
	
	 driver.findElement(By.id("P0_SEARCH_VALUE")).sendKeys(msisdn);
	
	 driver.findElement(By.id("P0_SEARCH")).submit();
	
 }catch(StaleElementReferenceException E)  {
				   
	 Select dropdown = new Select(driver.findElement(By.id("P0_SEARCH_TYPE")));
	
	 dropdown.selectByVisibleText("MSISDN");
	
	 driver.findElement(By.id("P0_SEARCH_VALUE")).clear();
	
	 driver.findElement(By.id("P0_SEARCH_VALUE")).sendKeys(msisdn);
	
	 driver.findElement(By.id("P0_SEARCH")).submit();
 }
}

//******************************************* Method to click Deactive tab *****************************************
public void click_deactive()
{
	WebElement deactive= driver.findElement(By.id("B13991264910408423"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(deactive).click().build().perform();
	
}
	
//********************************************** Method to re-enter MSISDN *************************************************
public void ReEnterMSISDN()
{
	WebElement MSISDN= driver.findElement(By.id("P21_MSISDN_2"));
	
	MSISDN.sendKeys(msisdn);
	
}
	
//********************************************** Method to enter CONFIRM message *************************************************
public void confirm()
{
	WebElement CONFIRM= driver.findElement(By.id("P21_I_CONFIRM"));
	
	CONFIRM.sendKeys("I CONFIRM");
	
}
	
//********************************************** Method to submit deletion request *************************************************
public void SUBMIT()
{
	WebElement sub= driver.findElement(By.id("SUBMIT"));
	
	Actions act= new Actions(driver);
	
	act.moveToElement(sub).click().build().perform();
	
}
		
//*********************************************** Method to check success message on MSISDN deactivation ************************
public void CheckSuccessMsg()
{
	String Msg= driver.findElement(By.id("echo-message")).getText();

	String Expected_msg= "Deactivation Successful";

	Assert.assertEquals(Msg, Expected_msg);
	
}

//************************************************ Method to view subscriber data after deactivation ****************************
public void View_subscriber_info()
{
	String Msg= driver.findElement(By.id("echo-message")).getText();
	
	String Expected_msg= "MSISDN is Deactivated";
	
	Assert.assertEquals(Msg, Expected_msg);
	
	WebElement element= driver.findElement(By.id("P2_SI_MSISDN"));
	
	Boolean info= element.isDisplayed();
	
	Assert.assertTrue(info);
	}
}
